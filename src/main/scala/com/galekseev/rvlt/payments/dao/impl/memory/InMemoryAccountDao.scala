package com.galekseev.rvlt.payments.dao.impl.memory

import java.util.concurrent.ConcurrentHashMap

import com.galekseev.rvlt.payments.dao.AccountDao
import com.galekseev.rvlt.payments.model.{Account, PaymentsException}
import com.typesafe.scalalogging.StrictLogging

class InMemoryAccountDao extends AccountDao with StrictLogging {
  private val accounts: ConcurrentHashMap[Long, Account] = new ConcurrentHashMap()

  override def createAccount(account: Account): Unit =
    if (accounts.putIfAbsent(account.id, account) == null) {
      throw PaymentsException(s"Account with id [${account.id}] already exists")
    }

  override def readAccount(id: Long): Option[Account] =
    Option(accounts.get(id))

  override def updateAccount(account: Account): Unit =
    if (accounts.computeIfPresent(account.id, (_, account) => account) == null) {
      throw PaymentsException(s"Account with id [${account.id}] does not exist")
    }

  override def deleteAccount(id: Long): Unit =
    if (accounts.remove(id) == null) {
      logger.warn(s"Account with id [$id] does not exist")
    }
}
