package com.galekseev.rvlt.payments.dao

import com.galekseev.rvlt.payments.model.Audit

import scala.concurrent.Future

trait AuditDao {
  def saveAudit(audit: Audit): Future[Unit]

  def findAudit(accountId: Long): Future[Seq[Audit]]
}


