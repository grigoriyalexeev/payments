package com.galekseev.rvlt.payments.dao

import com.galekseev.rvlt.payments.model.Account

trait AccountDao {
  // todo write JavaDoc
  def createAccount(account: Account): Unit
  def readAccount(id: Long): Option[Account]
  def updateAccount(account: Account): Unit
  def deleteAccount(id: Long): Unit
}
