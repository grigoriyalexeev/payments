package com.galekseev.rvlt.payments.dao.impl.memory

import java.time.LocalDateTime

import com.galekseev.rvlt.payments.dao.AuditDao
import com.galekseev.rvlt.payments.model.{Audit, TransferAudit}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class InMemoryAuditDao extends AuditDao {
  @volatile var transferAudits: List[TransferAudit] = List[TransferAudit]()

  override def saveAudit(audit: Audit): Future[Unit] = {
    Future {
      audit match {
        case transfer: TransferAudit =>
          transferAudits = transfer :: transferAudits
      }
    }
  }

  def findAudit(accountId: Long): Future[Seq[Audit]] = {
    implicit val localDateTimeOrdering: Ordering[LocalDateTime] =
      Ordering.fromLessThan((first: LocalDateTime, second: LocalDateTime) => first.isBefore(second))

    Future {
      val result = transferAudits
        .filter(transferAudit =>
          Seq(transferAudit.sourceAccountId, transferAudit.destinationAccountId)
            .contains(accountId)
        )
        .sortBy(transferAudit => transferAudit.processingTimestamp)
      result
    }
  }
}
