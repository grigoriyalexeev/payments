package com.galekseev.rvlt.payments.model

import javax.money.MonetaryAmount

sealed trait Account {
  def id: Long
  def balance: MonetaryAmount
}

final case class ImmutableAccount(id: Long, balance: MonetaryAmount) extends Account
