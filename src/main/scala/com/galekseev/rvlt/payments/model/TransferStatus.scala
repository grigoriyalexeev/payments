package com.galekseev.rvlt.payments.model

sealed trait TransferStatus
case object TransferRequested extends TransferStatus
case object TransferSuccess extends TransferStatus
final case class TransferFailure(reason: String) extends TransferStatus
final case class TransferError(reason: String) extends TransferStatus
