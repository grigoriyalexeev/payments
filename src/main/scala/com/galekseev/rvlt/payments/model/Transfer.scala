package com.galekseev.rvlt.payments.model

import javax.money.MonetaryAmount

final case class Transfer(sourceAccountId: Long, destinationAccountId: Long, amount: MonetaryAmount)