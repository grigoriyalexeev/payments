package com.galekseev.rvlt.payments.model

import java.time.LocalDateTime
import javax.money.MonetaryAmount

sealed trait Audit
final case class TransferAudit(processingTimestamp: LocalDateTime,
                               sourceAccountId: Long,
                               destinationAccountId: Long,
                               amount: MonetaryAmount,
                               status: TransferStatus
                              ) extends Audit
