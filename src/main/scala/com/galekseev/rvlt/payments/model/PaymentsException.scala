package com.galekseev.rvlt.payments.model

import scala.util.control.NoStackTrace

case class PaymentsException(message: String) extends Exception(message) with NoStackTrace
