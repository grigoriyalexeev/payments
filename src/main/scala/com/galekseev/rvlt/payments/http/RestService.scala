package com.galekseev.rvlt.payments.http

import java.time.LocalDateTime

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.galekseev.rvlt.payments.dao.AuditDao
import com.galekseev.rvlt.payments.model._
import com.galekseev.rvlt.payments.service.AccountService
import com.galekseev.rvlt.payments.util.serialization.ModelSerialization._
import com.typesafe.scalalogging.StrictLogging
import play.api.libs.json._

import scala.concurrent.Future
import scala.util.{Failure, Success}

class RestService(accountService: AccountService, auditService: AuditDao) extends StrictLogging {
  val route: Route =
    get {
      pathPrefix("account" / LongNumber) { id =>
        val maybeAccount: Future[Option[Account]] = accountService.getAccount(id)

        onSuccess(maybeAccount) {
          case Some(account: ImmutableAccount) => complete(account)
          case Some(account) => complete(HttpResponse(
            status = StatusCodes.InternalServerError,
            entity = s"Unmarshallable account type: ${account.getClass.getCanonicalName}")
          )
          case None       => complete(StatusCodes.NotFound)
        }
      } ~
        pathPrefix("audit" / LongNumber) { accountId =>
          val maybeAudit: Future[Seq[Audit]] = auditService.findAudit(accountId)

          onSuccess(maybeAudit) { audit =>
            complete(audit)
          }
        }
    } ~
      post {
        path("transfer") {
          entity(as[Transfer]) { transfer =>
            auditService.saveAudit(TransferAudit(
              LocalDateTime.now(),
              transfer.sourceAccountId,
              transfer.destinationAccountId,
              transfer.amount,
              TransferRequested
            ))

            val maybeTransferStatus = accountService.transfer(
              transfer.sourceAccountId,
              transfer.destinationAccountId,
              transfer.amount
            )
            onComplete(maybeTransferStatus) {
              case Success(transferStatus) =>
                transferStatus match {
                  case TransferRequested => complete(StatusCodes.Created)
                  case TransferSuccess => complete(StatusCodes.OK)
                  case status@TransferFailure(reason) =>
                    logger.info(s"Transfer [$transfer] has failed because of [$reason]")
                    complete(HttpResponse(status = StatusCodes.Forbidden, entity = Json.stringify(Json.toJson(status))))
                  case status@TransferError(reason) =>
                    logger.error(s"Transfer [$transfer] has failed because of [$reason]")
                    complete(HttpResponse(status = StatusCodes.InternalServerError, entity = Json.stringify(Json.toJson(status))))
                }

              case Failure(exception) =>
                logger.error(s"Transfer [$transfer] has failed", exception)
                auditService.saveAudit(TransferAudit(
                  LocalDateTime.now(),
                  transfer.sourceAccountId,
                  transfer.destinationAccountId,
                  transfer.amount,
                  TransferError(exception.getMessage)
                ))
                complete(StatusCodes.InternalServerError)
            }
          }
        }
      }
}
