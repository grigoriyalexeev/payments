package com.galekseev.rvlt.payments.http

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn

object HttpServer extends StrictLogging {
  def start(route: Route, port: Int): Unit = {
    // needed to run the route
    implicit val system: ActorSystem = ActorSystem()
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    // needed for the future map/flatmap in the end
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher

    val routeWithLogging: Route = logRequestResult(("request-response", Logging.DebugLevel))(route)
    val bindingFuture = Http().bindAndHandle(routeWithLogging, "localhost", port)

    // scalastyle:off
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    // scalastyle:on
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ ⇒ system.terminate()) // and shutdown when done
  }
}
