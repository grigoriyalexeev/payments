package com.galekseev.rvlt.payments.util.serialization.serializer

import javax.money.MonetaryAmount

import com.galekseev.rvlt.payments.model.{Account, ImmutableAccount}
import play.api.libs.json.Json.obj
import play.api.libs.json.{JsObject, JsSuccess, JsValue, OFormat}

class AccountSerializer extends OFormat[Account] {
  import AccountSerializer._

  implicit val monetaryAmountFormat: MonetaryAmountSerializer = new MonetaryAmountSerializer

  override def writes(o: Account): JsObject = o match {
    case account: ImmutableAccount => obj(
      idFieldName -> account.id,
      balanceFieldName -> account.balance
    )
  }

  override def reads(json: JsValue): JsSuccess[Account] = {
    val id = (json \ idFieldName).validate[Long].get
    val balance = (json \ balanceFieldName).validate[MonetaryAmount].get
    JsSuccess(ImmutableAccount(id, balance))
  }
}

object AccountSerializer {
  private val idFieldName = "id"
  private val balanceFieldName = "balance"
}
