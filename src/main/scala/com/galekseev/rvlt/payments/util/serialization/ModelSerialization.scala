package com.galekseev.rvlt.payments.util.serialization

import javax.money.MonetaryAmount

import com.galekseev.rvlt.payments.model._
import com.galekseev.rvlt.payments.util.serialization.serializer.{AccountSerializer, AuditSerializer, MonetaryAmountSerializer, TransferStatusSerializer}
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import play.api.libs.json._

object ModelSerialization extends PlayJsonSupport {
  implicit val monetaryAmountFormat: OFormat[MonetaryAmount] = new MonetaryAmountSerializer
  implicit val transferFormat: OFormat[Transfer] = Json.format[Transfer]
  implicit val accountFormat: OFormat[Account] = new AccountSerializer
  implicit val transferStatusFormat: OFormat[TransferStatus] = new TransferStatusSerializer
  implicit val transferAuditFormat: OFormat[Audit] = new AuditSerializer
}
