package com.galekseev.rvlt.payments.util.serialization.serializer

import java.math
import javax.money.MonetaryAmount

import org.javamoney.moneta.Money
import play.api.libs.json.Json.obj
import play.api.libs.json._

class MonetaryAmountSerializer extends OFormat[MonetaryAmount] {
  override def writes(o: MonetaryAmount): JsObject = {
    obj(
      "amount" → JsNumber(o.getNumber.numberValue(classOf[math.BigDecimal])),
      "currency" -> o.getCurrency.getCurrencyCode
    )
  }

  override def reads(json: JsValue): JsSuccess[MonetaryAmount] = {
    val amount = (json \ "amount").validate[math.BigDecimal].get
    val currency = (json \ "currency").validate[String].get
    JsSuccess(Money.of(amount, currency))
  }
}
