package com.galekseev.rvlt.payments.util.serialization.serializer

import com.galekseev.rvlt.payments.model._
import play.api.libs.json.Json.obj
import play.api.libs.json._

class TransferStatusSerializer extends OFormat[TransferStatus] {
  import TransferStatusSerializer._

  override def writes(o: TransferStatus): JsObject = o match {
    case TransferRequested => obj(StatusFieldName -> RequestedStatus)
    case TransferSuccess => obj(StatusFieldName -> SuccessStatus)
    case TransferFailure(reason) => obj(StatusFieldName -> FailureStatus, ReasonFieldName -> reason)
    case TransferError(reason) => obj(StatusFieldName -> ErrorStatus, ReasonFieldName -> reason)
  }

  override def reads(json: JsValue): JsSuccess[TransferStatus] = {
    val status = (json \ StatusFieldName).validate[String].get
    val maybeReason = (json \ ReasonFieldName).validateOpt[String].get
    JsSuccess(status match {
      case RequestedStatus => TransferRequested
      case SuccessStatus => TransferSuccess
      case FailureStatus => TransferFailure(maybeReason.get)
      case ErrorStatus => TransferError(maybeReason.get)
    })
  }
}

object TransferStatusSerializer {
  val StatusFieldName = "status"
  val ReasonFieldName = "reason"
  val RequestedStatus = "requested"
  val SuccessStatus = "success"
  val FailureStatus = "failure"
  val ErrorStatus = "error"
}
