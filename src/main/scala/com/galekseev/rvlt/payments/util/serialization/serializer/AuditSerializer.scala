package com.galekseev.rvlt.payments.util.serialization.serializer

import java.time.LocalDateTime
import javax.money.MonetaryAmount

import com.galekseev.rvlt.payments.model.{Audit, TransferAudit, TransferStatus}
import play.api.libs.json.Json.obj
import play.api.libs.json._

class AuditSerializer extends OFormat[Audit] {
  import AuditSerializer._

  implicit val monetaryAmountSerializer: MonetaryAmountSerializer = new MonetaryAmountSerializer
  implicit val transferStatusSerializer: TransferStatusSerializer = new TransferStatusSerializer

  override def writes(o: Audit): JsObject = o match {
    case audit: TransferAudit =>
      obj(
        processingTimestampFieldName -> audit.processingTimestamp,
        sourceAccountIdFieldName -> audit.sourceAccountId,
        destinationAccountIdFieldName -> audit.destinationAccountId,
        amountFieldName -> audit.amount,
        statusFieldName -> audit.status
      )
  }

  override def reads(json: JsValue): JsSuccess[Audit] = {
    val processingTimestamp = (json \ processingTimestampFieldName).validate[LocalDateTime].get
    val sourceAccountId = (json \ sourceAccountIdFieldName).validate[Long].get
    val destinationAccountId = (json \ destinationAccountIdFieldName).validate[Long].get
    val amount = (json \ amountFieldName).validate[MonetaryAmount].get
    val status = (json \ statusFieldName).validate[TransferStatus].get

    JsSuccess(TransferAudit(processingTimestamp, sourceAccountId, destinationAccountId, amount, status))
  }
}

object AuditSerializer {
  val processingTimestampFieldName = "processingTimestamp"
  val sourceAccountIdFieldName = "sourceAccountId"
  val destinationAccountIdFieldName = "destinationAccountId"
  val amountFieldName = "amount"
  val statusFieldName = "status"
}
