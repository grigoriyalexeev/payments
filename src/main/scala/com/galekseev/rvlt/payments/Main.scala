package com.galekseev.rvlt.payments

import com.galekseev.rvlt.payments.dao.impl.memory.{InMemoryAccountDao, InMemoryAuditDao}
import com.galekseev.rvlt.payments.http.{HttpServer, RestService}
import com.galekseev.rvlt.payments.service.impl.{DefaultAccountService, InMemoryLongIdGenerator}
import com.typesafe.config.ConfigFactory
import org.javamoney.moneta.Money

import scala.concurrent.Future

object Main extends App {
  private val httpPortPropertyName = "http.port"
  private val conf = ConfigFactory.load
  private val accountDao = new InMemoryAccountDao
  private val auditDao = new InMemoryAuditDao
  private val accountIdGenerator = new InMemoryLongIdGenerator
  private val accountService = new DefaultAccountService(accountDao, auditDao, accountIdGenerator)

  initializeAccounts()

  HttpServer.start(
    new RestService(
      accountService,
      auditDao
    ).route,
    conf.getInt(httpPortPropertyName)
  )

  // todo: move to JSON, tests
  def initializeAccounts(): Future[Long] = {
    accountService.createAccount(Money.of(100, "RUB"))
    accountService.createAccount(Money.of(200, "USD"))
    accountService.createAccount(Money.of(300, "GBP"))
  }
}
