package com.galekseev.rvlt.payments.service.impl

import java.util.concurrent.atomic.AtomicLong

import com.galekseev.rvlt.payments.service.IdGenerator

class InMemoryLongIdGenerator extends IdGenerator[Long] {
  private val accountIdsCounter = new AtomicLong(0L)

  override def generate(): Long = accountIdsCounter.incrementAndGet()
}
