package com.galekseev.rvlt.payments.service

trait IdGenerator[T] {
  def generate(): T
}
