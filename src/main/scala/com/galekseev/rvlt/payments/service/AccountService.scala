package com.galekseev.rvlt.payments.service

import javax.money.MonetaryAmount

import com.galekseev.rvlt.payments.model.{Account, TransferStatus}

import scala.concurrent.Future

trait AccountService {
  def getAccount(itemId: Long): Future[Option[Account]]

  def createAccount(initialBalance: MonetaryAmount): Future[Long]

  def transfer(sourceAccountId: Long, destinationAccountId: Long, amount: MonetaryAmount): Future[TransferStatus]
}
