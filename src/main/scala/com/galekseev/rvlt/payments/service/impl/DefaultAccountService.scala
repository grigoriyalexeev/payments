package com.galekseev.rvlt.payments.service.impl

import java.util.concurrent.locks.{ReadWriteLock, ReentrantReadWriteLock}
import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}
import javax.money.MonetaryAmount
import javax.money.convert.MonetaryConversions.getConversion

import com.galekseev.rvlt.payments.dao.{AccountDao, AuditDao}
import com.galekseev.rvlt.payments.model._
import com.galekseev.rvlt.payments.service.{AccountService, IdGenerator}
import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.control.NonFatal

class DefaultAccountService(accountDao: AccountDao, auditDao: AuditDao, accountIdGenerator: IdGenerator[Long])
  extends AccountService with StrictLogging {

  private val accountMutexes: ConcurrentMap[Long, ReadWriteLock] = new ConcurrentHashMap()

  override def getAccount(id: Long): Future[Option[Account]] =
    Future {
      callReadLocked(id) { () =>
        accountDao.readAccount(id)
      }
    }

  override def createAccount(initialBalance: MonetaryAmount): Future[Long] = {
    require(initialBalance.isPositiveOrZero, "Initial balance must be non-negative")
    Future {
      // todo: use UUID.randomUUID() ?
      val newAccountId = accountIdGenerator.generate()

      callWriteLocked(newAccountId) { () =>
        accountDao.createAccount(ImmutableAccount(newAccountId, initialBalance))
        newAccountId
      }
    }
  }

  override def transfer(sourceAccountId: Long, destinationAccountId: Long, amount: MonetaryAmount): Future[TransferStatus] = {
    require(amount.isPositive, "Initial balance must be positive")
    Future {
      try {

        callWriteLocked(sourceAccountId, destinationAccountId) { () =>
          val source = accountDao.readAccount(sourceAccountId)
            .getOrElse(throw PaymentsException(s"Source account with id [$sourceAccountId] does not exist"))
          val destination = accountDao.readAccount(destinationAccountId)
            .getOrElse(throw PaymentsException(s"Destination account with id [$destinationAccountId] does not exist"))
          if (!source.balance.isGreaterThanOrEqualTo(amount))
            throw PaymentsException(s"Insufficient funds on account with id [$sourceAccountId]")

          try {
            accountDao.updateAccount(ImmutableAccount(
              sourceAccountId,
              source.balance.subtract(
                amount.`with`(getConversion(source.balance.getCurrency))
              )
            ))
            accountDao.updateAccount(ImmutableAccount(
              destinationAccountId,
              destination.balance.add(
                amount.`with`(getConversion(destination.balance.getCurrency))
              )
            ))
            TransferSuccess

          } catch {
            case NonFatal(e) =>
              accountDao.updateAccount(source)
              accountDao.updateAccount(destination)
              TransferError(s"Transaction has been rolled back. [$e]")
            case fatal: Throwable =>
              accountDao.updateAccount(source)
              accountDao.updateAccount(destination)
              logger.info(s"Transaction has been rolled back")
              throw fatal
          }
        }

      } catch {
        case e: PaymentsException => TransferFailure(e.getMessage)
        case NonFatal(e) =>
          logger.error(s"Error while transferring from account [$sourceAccountId] to [$destinationAccountId]", e)
          TransferError(e.toString)
      }
    }
  }

  private def callReadLocked[T](accountId: Long)(callable: () => T): T = {
    accountMutexes.putIfAbsent(accountId, new ReentrantReadWriteLock())
    val readLock = accountMutexes.get(accountId).readLock()
    try {
      readLock.lock()

      callable.apply()

    } finally {
      readLock.unlock()
    }
  }

  private def callWriteLocked[T](accountId: Long)(callable: () => T): T = {
    accountMutexes.putIfAbsent(accountId, new ReentrantReadWriteLock())
    val writeLock = accountMutexes.get(accountId).writeLock()
    try {
      writeLock.lock()

      callable.apply()

    } finally {
      writeLock.unlock()
    }
  }

  private def callWriteLocked[T](firstAccountId: Long, secondAccountId: Long)(callable: () => T): T = {
    accountMutexes.putIfAbsent(firstAccountId, new ReentrantReadWriteLock())
    accountMutexes.putIfAbsent(secondAccountId, new ReentrantReadWriteLock())
    val firstWriteLock = accountMutexes.get(Math.min(firstAccountId, secondAccountId)).writeLock()
    val secondWriteLock = accountMutexes.get(Math.min(firstAccountId, secondAccountId)).writeLock()
    try {
      firstWriteLock.lock()
      secondWriteLock.lock()

      callable.apply()

    } finally {
      secondWriteLock.unlock()
      firstWriteLock.unlock()
    }
  }
}
