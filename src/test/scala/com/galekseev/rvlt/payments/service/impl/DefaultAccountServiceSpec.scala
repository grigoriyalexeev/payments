package com.galekseev.rvlt.payments.service.impl

import javax.money.Monetary
import javax.money.convert.MonetaryConversions.getConversion

import com.galekseev.rvlt.payments.AsyncUnitSpec
import com.galekseev.rvlt.payments.dao.{AccountDao, AuditDao}
import com.galekseev.rvlt.payments.model._
import com.galekseev.rvlt.payments.service.IdGenerator
import org.javamoney.moneta.Money

import scala.concurrent.Future
import scala.language.reflectiveCalls
import scala.util.{Failure, Success}

class DefaultAccountServiceSpec extends AsyncUnitSpec {

  "An AccountService" when {
    "no accounts in AccountDao" should {
      "eventually return None on looking up an account" in {
        val f = emptyAccountDaoFixture

        f.defaultAccountService.getAccount(1L) map { account =>

          assert(account === None)
        }
      }

      "eventually return TransferFailure on transferring $10 from the account 1 to 2" in {
        val f = emptyAccountDaoFixture

        f.defaultAccountService.transfer(1L, 2L, Money.of(10, Monetary.getCurrency("USD"))
        ) map { transferStatus =>

          assert(transferStatus.isInstanceOf[TransferFailure])
        }
      }

      "eventually call account DAO with expected account on creating a new account" in {
        val f = emptyAccountDaoFixture

        val initialBalance = Money.of(1, "EUR")
        f.defaultAccountService.createAccount(initialBalance) map { _ =>

          (f.accountDaoStub.createAccount (_: Account))
            .verify(ImmutableAccount(f.GeneratedAccountId, initialBalance))
          // async tests work in ScalaTest only if an Assertion is returned
          succeed
        }
      }

      "eventually throws IllegalArgumentException on creating a new account with negative balance" in {
        val f = emptyAccountDaoFixture

        assertThrows[IllegalArgumentException](
          f.defaultAccountService.createAccount(Money.of(-1, "USD")).onComplete {
            case Success(id) => id
            case Failure(throwable) => throw throwable
          }
        )
      }
    }

    "accounts 1, 2, 3 and 4 exist in AccountDao" should {
      "eventually return Some(account 1) on looking up the account 1" in {
        val f = prefilledAccountDaoFixture

        f.defaultAccountService.getAccount(f.account1.id) map { account =>

          assert(account === Some(f.account1))
        }
      }

      "eventually return TransferSuccess on transferring amount less than the account 2's balance from the account 2 to 3" +
        "and call the account DAO to update accounts 2 and 3 with the expected balances" in {
        val f = prefilledAccountDaoFixture
        val account2 = f.account2
        val account3 = f.account3
        val account2balance = account2.balance
        val account3balance = account3.balance
        val transferAmount = Money.of(1, account2balance.getCurrency)
        require(account2balance.isGreaterThan(transferAmount))
        val expectedAccount2Balance = account2balance.subtract(transferAmount)
        val expectedAccount3Balance = account3balance.add(transferAmount.`with`(getConversion(account3balance.getCurrency)))

        f.defaultAccountService.transfer(account2.id, account3.id, transferAmount) map { transferStatus =>

          (f.accountDaoStub.updateAccount (_: Account))
            .verify(ImmutableAccount(account2.id, expectedAccount2Balance))
          (f.accountDaoStub.updateAccount (_: Account))
            .verify(ImmutableAccount(account3.id, expectedAccount3Balance))
          assert(transferStatus === TransferSuccess)
        }
      }

      "eventually call account DAO with expected account on creating a new account" in {
        val f = emptyAccountDaoFixture

        val initialBalance = Money.of(1, "EUR")
        f.defaultAccountService.createAccount(initialBalance) map { _ =>

          (f.accountDaoStub.createAccount (_: Account))
            .verify(ImmutableAccount(f.GeneratedAccountId, initialBalance))
          // async tests work in ScalaTest only if an Assertion is returned
          succeed
        }
      }
    }
  }

  def emptyAccountDaoFixture = new {
    val accountDaoStub: AccountDao = stub[AccountDao]
    val auditDaoMock: AuditDao = mock[AuditDao]
    val accountIdGeneratorStub: IdGenerator[Long] = stub[IdGenerator[Long]]
    val defaultAccountService: DefaultAccountService = new DefaultAccountService(accountDaoStub, auditDaoMock, accountIdGeneratorStub)

    val GeneratedAccountId = 1L

    (accountDaoStub.readAccount (_: Long)).when(*).returns(None)
    (accountDaoStub.createAccount (_: Account)).when(*).returns(())
    (accountIdGeneratorStub.generate _).when().returns(GeneratedAccountId)
  }

  def prefilledAccountDaoFixture = new {
    val accountDaoStub: AccountDao = stub[AccountDao]
    val auditDaoMock: AuditDao = mock[AuditDao]
    val accountIdGeneratorStub: IdGenerator[Long] = stub[IdGenerator[Long]]
    val defaultAccountService: DefaultAccountService = new DefaultAccountService(accountDaoStub, auditDaoMock, accountIdGeneratorStub)

    val GeneratedAccountId = 1L

    val account1 = ImmutableAccount(1L, Money.of(0, "GBP"))
    val account2 = ImmutableAccount(2L, Money.of(10, "GBP"))
    val account3 = ImmutableAccount(3L, Money.of(100, "USD"))
    val account4 = ImmutableAccount(4L, Money.of(1000, "JPY"))
    (accountDaoStub.readAccount (_: Long))
      .when(account1.id)
      .returns(Some(account1))
    (accountDaoStub.readAccount (_: Long))
      .when(account2.id)
      .returns(Some(account2))
    (accountDaoStub.readAccount (_: Long))
      .when(account3.id)
      .returns(Some(account3))
    (accountDaoStub.readAccount (_: Long))
      .when(account4.id)
      .returns(Some(account4))

    (accountDaoStub.createAccount (_: Account))
      .when(where { (account: Account) =>
        Seq(account1.id, account2.id, account3.id, account4.id).contains(account.id)
      })
      .returns(Future.failed(PaymentsException("")))
    (accountDaoStub.createAccount (_: Account))
      .when(where { (account: Account) =>
        ! Seq(account1.id, account2.id, account3.id, account4.id).contains(account.id)
      })
      .returns(Future.successful(()))

    (accountDaoStub.updateAccount (_: Account))
      .when(where { (account: Account) =>
        Seq(account1.id, account2.id, account3.id, account4.id).contains(account.id)
      })
      .returns(Future.successful(()))

    (accountIdGeneratorStub.generate _).when().returns(GeneratedAccountId)
  }
}
