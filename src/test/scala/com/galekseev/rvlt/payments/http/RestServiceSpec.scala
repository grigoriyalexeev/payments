package com.galekseev.rvlt.payments.http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.galekseev.rvlt.payments.dao.AuditDao
import com.galekseev.rvlt.payments.model.{Account, ImmutableAccount}
import com.galekseev.rvlt.payments.service.AccountService
import com.galekseev.rvlt.payments.util.serialization.ModelSerialization._
import com.typesafe.scalalogging.StrictLogging
import org.javamoney.moneta.Money
import org.scalamock.scalatest.MockFactory
import org.scalatest.{Matchers, WordSpec}
import play.api.libs.json._

import scala.concurrent.Future

class RestServiceSpec extends WordSpec with Matchers with ScalatestRouteTest with MockFactory with StrictLogging {
  val accountService: AccountService = stub[AccountService]
  val auditDao: AuditDao = stub[AuditDao]
  val testAccount: Account = ImmutableAccount(1L, Money.of(100, "RUB"))
  (accountService.getAccount _)
    .when(1L)
    .returns(Future.successful(Some(testAccount)))
  val route: Route = new RestService(accountService, auditDao).route

  "RestService" when {
    "requested to get an existing account" should {
      "respond with a JSON representation of this account" in {
        Get(s"/account/${testAccount.id}") ~> route ~> check {

          entityAs[JsObject].as[Account] shouldEqual testAccount
        }
      }
    }

    "requested an unknown path" should {
      "leave the request unhandled" in {
        Get("/foo") ~> route ~> check {

          handled shouldBe false
        }
      }
    }

    "requested to put an audit" should {
      "return a MethodNotAllowed error" in {
        Put("/audit") ~> Route.seal(route) ~> check {

          status shouldEqual StatusCodes.MethodNotAllowed
        }
      }
    }
  }
}
