package com.galekseev.rvlt.payments

import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest._

abstract class AsyncUnitSpec
  extends AsyncWordSpec
    with Matchers
    with AsyncMockFactory
