name := "rvlt-payments"
organization := "com.galekseev"
version := "0.1"
scalaVersion := "2.12.3"

scalacOptions := Seq(
  "-Xfatal-warnings",
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xlint",
  "-Yno-adapted-args",
  "-Xfuture",
  "-encoding", "utf8",
  "-target:jvm-1.8"
)

libraryDependencies ++= {
  val mainDependencies = Seq(
    // HTTP
    "com.typesafe.akka" %% "akka-http" % "10.0.10",

    // money
    "org.javamoney" % "moneta" % "1.0",

    // JSON
    "com.typesafe.play" %% "play-json" % "2.6.5",
    "de.heikoseeberger"             %%  "akka-http-play-json"               % "1.12.0",

    // logging
    "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
    "ch.qos.logback" % "logback-classic" % "1.2.3"
  )

  val testDependencies = Seq(
    "org.scalatest" %% "scalatest" % "3.0.4",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0",
    "com.typesafe.akka" %% "akka-http-testkit" % "10.0.10"
  )

  mainDependencies ++ testDependencies.map(_ % Test)
}

scalastyleConfig := file("src/main/resources/scalastyle-config.xml")
scalastyleFailOnError := false
lazy val testScalastyle = taskKey[Unit]("testScalastyle")
testScalastyle := scalastyle.in(Compile).toTask("").value
(test in Test) := ((test in Test) dependsOn testScalastyle).value

assemblyJarName := name.value + ".jar"
