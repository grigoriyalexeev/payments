# Money Transfer

## Requires
* JDK 8
* SBT

## Key dependencies
* akka-http
* play-json
* moneta (to be replaced with a custom model and service)
* scala-logging, logback
* scalatest, scalamock

## Run instructions

$ git clone git@bitbucket.org:grigoriyalexeev/rvlt-payments.git  
$ cd rvlt-payments  
$ sbt assembly  
$ java -jar target/scala-2.12/rvlt-payments.jar  

The default port is 8080. Set the 'http.port' system variable to use another port.

## API usage

### Transfer USD 1 from account with id 2 to account with id 3

Request:

POST /transfer
{
    "sourceAccountId": 2,
        "destinationAccountId":3,
        "amount": {
            "amount": 1,
            "currency": "USD"
        }
}

Response:

HTTP 201                        (if transfer has completed successfully)  
HTTP 403 {"reason": "{reason}"} (if transfer has failed due to a client error)  
HTTP 500                        (if transfer has failed due to a server error)  

#### Get an audit trail for an account with id {id}

Request:

GET /audit/{id}

Response:

        HTTP 200
            [
            {
                "processingTimestamp":"2017-09-04T16:36:48.219",
                    "sourceAccountId":1,
                    "destinationAccountId":2,
                    "money":{
                        "amount":1,
                        "currency":"USD"
                    },
                    "result":{
                        "status":"requested"
                    }
            },
            {
                "processingTimestamp":"2017-09-04T16:36:48.41",
                "sourceAccountId":1,
                "destinationAccountId":2,
                "money":{
                    "amount":1,
                    "currency":"USD"
                },
                "result":{
                    "status":"success"
                }
            },
            {
                "processingTimestamp":"2017-09-04T16:36:51.695",
                "sourceAccountId":1,
                "destinationAccountId":2,
                "money":{
                    "amount":1E+3,
                    "currency":"RUB"
                },
                "result":{
                    "status":"failure",
                    "reason":"Insufficient funds"
                }
            },
            {
                "processingTimestamp":"2017-09-04T16:36:51.695",
                "sourceAccountId":1,
                "destinationAccountId":2,
                "money":{
                    "amount":1E+3,
                    "currency":"RUB"
                },
                "result":{
                    "status":"requested"
                }
            },
            {
                "processingTimestamp":"2017-09-04T16:36:54.598",
                "sourceAccountId":1,
                "destinationAccountId":4,
                "money":{
                    "amount":1E+3,
                    "currency":"RUB"
                },
                "result":{
                    "status":"failure",
                    "reason":"Account [4] does not exist"
                }
            },
            {
                "processingTimestamp":"2017-09-04T16:36:54.598",
                "sourceAccountId":1,
                "destinationAccountId":4,
                "money":{
                    "amount":1E+3,
                    "currency":"RUB"
                },
                "result":{
                    "status":"requested"
                }
            }
        ]

#### Get a statement for an account with id {id}

Request:

            GET /account/{id}

Response:

        HTTP 200
        {
            "id":1,
                "money":{
                    "amount":34.553150638953355457276875352704,
                    "currency":"RUB"
                }
        }
